import os
from django.test import TestCase, Client
from django.urls import resolve
from .views import landing

class Story8UnitTest(TestCase):
    def test_url_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)
    def test_method_view(self):
        found = resolve('/')
        self.assertEqual(found.func, landing)
    def test_template_used(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'landing.html')
    def test_template_sesuai(self):
        response = Client().get('/')
        self.assertIn("Search</button>", response.content.decode())

import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from story8.settings import BASE_DIR
from time import sleep


class VisitorTest(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome(executable_path=os.path.join(BASE_DIR,'chromedriver'), chrome_options=chrome_options)

    def tearDown(self):
        self.browser.quit()
    
    def test_search(self):
        driver = self.browser
        driver.get('http://localhost:8000/')
        input = driver.find_element_by_id("input")
        input.send_keys("prabowi");
        input.send_keys(Keys.RETURN)
        button = driver.find_element_by_id("but")
        button.click()
        daftar = driver.find_element_by_class_name("data-buku")
        sleep(5)
        self.assertIn("Prabowo", driver.page_source)
    
$(document).ready(function () {
    $.ajax({
        url: "https://www.googleapis.com/books/v1/volumes?q=belajar pemrograman web",
        success: function (result) {
            $(".isi").empty();
            for (i = 0; i < result.items.length; i++) {
                var author;
                if (result.items[i].volumeInfo.authors == undefined) author = "no name";
                else author = result.items[i].volumeInfo.authors;
                var img;
                if (!('imageLinks' in result.items[i].volumeInfo)) img = "https://img.icons8.com/wired/64/000000/no-image.png";
                else img = result.items[i].volumeInfo.imageLinks.thumbnail;
                $("#isi").append(
                    "<tr class='isi'><th>" + (i + 1) + "</th>" +
                    "<td>" + result.items[i].volumeInfo.title +
                    "</td><td>" + author +
                    "</td><td><img src='" + img +
                    "'></img></td></tr>"
                );
            }
        }
    })
    $("#input").keypress("enterKey", function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            $.ajax({
                url: "https://www.googleapis.com/books/v1/volumes?q=" + $("#input").val(),
                success: function (result) {
                    $(".isi").empty();
                    for (i = 0; i < result.items.length; i++) {
                        var author;
                        if (result.items[i].volumeInfo.authors == undefined) author = "no name";
                        else author = result.items[i].volumeInfo.authors;
                        var img;
                        if (!('imageLinks' in result.items[i].volumeInfo)) img = "https://img.icons8.com/wired/64/000000/no-image.png";
                        else img = result.items[i].volumeInfo.imageLinks.thumbnail;
                        $("#isi").append(
                            "<tr class='isi'><th>" + (i + 1) + "</th>" +
                            "<td>" + result.items[i].volumeInfo.title +
                            "</td><td>" + author +
                            "</td><td><img src='" + img +
                            "'></img></td></tr>"
                        );
                    }
                }
            })
        }
    })
    $("button").click( function() {
        $.ajax({
            url: "https://www.googleapis.com/books/v1/volumes?q=" + $("#input").val(),
            success: function (result) {
                $(".isi").empty();
                for (i = 0; i < result.items.length; i++) {
                    var author;
                    if (result.items[i].volumeInfo.authors == undefined) author = "no name";
                    else author = result.items[i].volumeInfo.authors;
                    var img;
                    if (!('imageLinks' in result.items[i].volumeInfo)) img = "https://img.icons8.com/wired/64/000000/no-image.png";
                    else img = result.items[i].volumeInfo.imageLinks.thumbnail;
                    $("#isi").append(
                        "<tr class='isi'><th>" + (i + 1) + "</th>" +
                        "<td>" + result.items[i].volumeInfo.title +
                        "</td><td>" + author +
                        "</td><td><img src='" + img +
                        "'></img></td></tr>"
                    );
                }
            }
        })
    })
})
